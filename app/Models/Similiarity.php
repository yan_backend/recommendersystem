<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Similiarity extends Model
{
    protected $fillable = ['game_id_1', 'game_id_2', 'similiarity'];
}
