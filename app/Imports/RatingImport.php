<?php

namespace App\Imports;

use App\Models\Game;
use App\Models\Rating;
use App\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithHeadings;

class RatingImport implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $rows)
    {
        $gameNames = array_keys($rows[0]->toArray());
        $gameNames = array_slice($gameNames, 4);
        $gameNames = array_slice($gameNames, 0, 10);

        foreach ($rows as $key => $row) {
            $user = $this->createUser($row);
            foreach ($gameNames as $key => $gameName) {
                if ($row[$gameName] && !is_null($row[$gameName])) {
                    $gameId = $this->getGameId($gameName);

                    $user->save();
                    // dd($user->id);

                    Rating::create([
                        'game_id' => $gameId,
                        'user_id' => $user->id,
                        'rating'  => $row[$gameName]
                    ]);
                }
            }
        }
    }

    public function getGameId($gameName)
    {
        $gameName = implode(' ', explode('_', $gameName));

        return Game::whereName($gameName)->first()->id;
    }

    public function createUser($row)
    {
        return User::query()->where('email', $row['email_address'])->first() ?: factory(User::class)->make([
            'email'        => $row['email_address'],
            'name'         => $row['nama'],
            'phone_number' => $row['no_hp']
        ]);
    }
}
