<?php

namespace App\Recommender;

use App\Models\Game;
use App\Models\Prediction;
use App\Models\Rating;
use App\User;
use Illuminate\Support\Facades\Log;
use Psy\Command\DumpCommand;

class CollaborativeFiltering
{
    private $similiarities = [];
    private $gameIds = [];

    public function __construct()
    {
        $this->clearSimiliarities();
    }

    private function clearSimiliarities()
    {
        $this->similiarities = [];
    }

    public function makeRecommendation()
    {
        $ratings = $averageRatings = [];

        $this->gameIds = $comparedGameIds = $this->getAllGameId();

        array_shift($comparedGameIds);
        $counter = 0;

        foreach ($this->gameIds as $key => $gameId) {
            foreach ($comparedGameIds as $keyComparedGame => $comparedGameId) {
                $userIds = $this->getUserIds($gameId, $comparedGameId);

                if ($userIds) {
                    foreach ($userIds as $keyUserId => $userId) {
                        $ratings = $ratings ?? [];
                        array_push($ratings, $this->getUserRatingForComparasion($userId, $gameId, $comparedGameId));
                        $averageRatings[$keyUserId] = $this->getAverageUserRating($userId);
                        // if (count($ratings) == 4) {
                        //     $counter++;
                        //     dump("user Id : $userId", "counter : $counter");
                        // }
                        // $ratings = [];
                    }

                    $this->similiarity($ratings, $averageRatings, $gameId, $comparedGameId);
                }

                unset($ratings);
                unset($averageRatings);
            }

            array_shift($comparedGameIds);
        }
        // dd('stop');
    }

    private function similiarity($userRatings, $averageRatings, $gameId1, $gameId2)
    {
        $numerator      = 1;
        $usernumerator  = 0;
        $userDividerArr = [0, 0];

        foreach ($userRatings as $key => $currentUserRatings) {
            foreach ($currentUserRatings as $keyRating => $rating) {

                try {
                    $numerator  *= $rating - $averageRatings[$key];

                    $userDividerArr[$keyRating] += pow(($rating - $averageRatings[$key]), 2);
                } catch (\Throwable $th) {
                    // dump('error', $currentUserRatings, "game Id 1 : $gameId1", "game Id 2 : $gameId2");
                }
            }

            $usernumerator += $numerator;

            $numerator = 1;
        }

        $userDivider = sqrt($userDividerArr[0]) * sqrt($userDividerArr[1]);

        $similiarity = $usernumerator == 0
            ? 0
            : $usernumerator / $userDivider;

        $userDividerArr = [0, 0];

        if ($similiarity >= 0.5) {
            $this->addSimiliarity($similiarity, $gameId1, $gameId2);
        }
    }

    public function MAEResult($predictions, $actualRatings)
    {
        $numerator = 0;

        foreach ($actualRatings as $key => $rating) {
            $numerator += $predictions[$key]['prediction_score'] - $rating;
        }

        return $numerator / count($predictions);
    }

    public function getSimiliarities()
    {
        return $this->similiarities;
    }

    public function makePrediction($userId)
    {
        if (is_array($userId)) {
            foreach ($userId as $key => $id) {
                $this->predict($id);
            }
        } else $this->predict($userId);
    }

    private function predict($userId)
    {
        $ratedGame      = $this->getAllCurrentUserRatings($userId);
        $unratedGameIds = $this->getUnratedGameIds($ratedGame);

        $numerator = $divider = $prediction = 0;

        foreach ($unratedGameIds as $keyUnratedGameId => $unratedGameId) {
            $numerator = $divider = 0;

            foreach ($ratedGame as $keyGameId => $gameId) {
                $currentSimiliarity = $this->getCurrentSimiliarity($unratedGameId, $gameId);

                if ($currentSimiliarity) {
                    $userRating = $this->getSelectedtUserRating($gameId, $userId);

                    $divider   += $currentSimiliarity[0]['similiarity'];

                    if ($userRating) {
                        $numerator += $userRating->rating * $currentSimiliarity[0]['similiarity'];
                    }
                }
            }

            $this->savePrediction($numerator, $divider, $userId, $unratedGameId);
        }
    }

    public function calculateSimiliarity($gameId, $unratedGameId, $userId)
    {
        // dump($gameId, $unratedGameId, $userId);
    }

    private function addSimiliarity($similiarity, $gameId1, $gameId2)
    {
        $this->similiarities[] = [
            'game_id_1'   => $gameId1,
            'game_id_2'   => $gameId2,
            'similiarity' => $similiarity,
        ];
    }

    private function getCurrentSimiliarity($unratedGameId, $gameId)
    {
        return array_values(array_filter($this->similiarities, function ($similiarity) use ($unratedGameId, $gameId) {
            if (($similiarity['game_id_1'] == $unratedGameId && $similiarity['game_id_2'] == $gameId) ||
                ($similiarity['game_id_1'] == $gameId && $similiarity['game_id_2'] == $unratedGameId)
            ) {
                return $similiarity;
            }
        }));
    }

    private function savePrediction($numerator, $divider, $userId, $unratedGameId)
    {
        if ($numerator != 0 && $divider != 0) {
            Prediction::query()
                ->firstOrCreate(
                    [
                        'user_id' => $userId,
                        'game_id' => $unratedGameId,
                    ],
                    [
                        'prediction_score' => round($numerator / $divider)
                    ]
                );
        }
    }

    private function getSelectedtUserRating($gameId, $userId)
    {
        return Rating::where([
            ['game_id', '=', $gameId],
            ['user_id', '=', $userId],
        ])->first();
    }

    private function getAllCurrentUserRatings($userId)
    {
        return Rating::where('user_id', $userId)->get()->pluck('game_id')->toArray();
    }

    private function getUnratedGameIds($ratedGame)
    {
        return array_values(array_filter($this->gameIds, function ($gameId) use ($ratedGame) {
            return !in_array($gameId, $ratedGame);
        }));
    }

    private function getUserIds($gameId, $comparedGameId)
    {
        return Rating::select('user_id')->where('game_id', $gameId)
            ->orWhere('game_id', $comparedGameId)
            ->groupBy('user_id')
            ->havingRaw('COUNT(user_id) > ?', [1])
            ->get()
            ->pluck('user_id')
            ->toArray();
    }

    private function getUserRatingForComparasion($userId, $gameId, $comparedGameId)
    {
        $ratings = Rating::where('user_id', $userId)
            ->whereIn('game_id', [$gameId, $comparedGameId])
            ->get()
            ->pluck('rating')
            ->toArray();

        if (count($ratings) == 4) {
            Log::info(Rating::where('user_id', $userId)
                ->whereIn('game_id', [$gameId, $comparedGameId])
                ->toSql());
            Log::info("game id 1 : $gameId");
            Log::info("game id 2 : $comparedGameId");
            Log::info("user id : $userId");
            Log::info("=====================");
        }
        return $ratings;
    }

    private function getAverageUserRating($userId)
    {
        return Rating::where('user_id', $userId)
            ->avg('rating');
    }

    private function getAllGameId()
    {
        return Game::get()->pluck('id')->toArray();
    }
}
