<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Prediction;
use Illuminate\Http\Request;

class PredictionController extends Controller
{
    public function index()
    {
        $predictions = Prediction::with(['user', 'game'])->get();

        return view('admin.prediction.index', compact('predictions'));
    }
}
