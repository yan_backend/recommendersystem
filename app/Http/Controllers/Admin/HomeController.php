<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Game;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $gameCount = Game::count();

        $userCount = User::query()
            ->where('role', 0)->count();

        return view('admin.index', compact('gameCount', 'userCount'));
    }
}
