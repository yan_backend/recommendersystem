<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\ImportRequest;
use App\Imports\RatingImport;
use App\Models\Rating;
use Maatwebsite\Excel\Facades\Excel;


class ImportController extends Controller
{
    public function index()
    {
        Rating::query()->truncate();
        dd(route('admin.rating.index'));

        return view('admin.import');
    }

    public function import(ImportRequest $request)
    {
        // dd(route('admin.rating.index'));
        Excel::import(new RatingImport, $request->file('excel'));
        return redirect()->route('admin.rating.index');
    }
}
