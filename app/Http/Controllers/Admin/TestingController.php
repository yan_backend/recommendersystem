<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Game;
use App\Models\Prediction;
use App\Recommender\CollaborativeFiltering;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class TestingController extends Controller
{
    private $gameNames;

    private $dataTesting = [
        [
            'name'      => 'M WILDAN FATURAHMAN',
            'game_name' => 'PUBG Mobile',
            'rating'    => 5,
        ],
        [
            'name'      => 'M.Danil AlFikrah',
            'game_name' => 'Lineage 2 Revolution',
            'rating'    => 5,
        ],
        [
            'name'      => 'Fitrahdin',
            'game_name' => 'Call Of Duty',
            'rating'    => 4,
        ],
        [
            'name'      => 'rahman bayu widhakdo',
            'game_name' => 'League Of Legends',
            'rating'    => 4,
        ],
        [
            'name'      => 'Aan saputra',
            'game_name' => 'PUBG Mobile',
            'rating'    => 3,
        ],
        [
            'name'      => 'satria',
            'game_name' => 'Among Us',
            'rating'    => 3,
        ],
        [
            'name'      => 'MULIAWAN',
            'game_name' => 'Call Of Duty',
            'rating'    => 5,
        ],
        [
            'name'      => 'Aidil Adhar',
            'game_name' => 'Free Fire',
            'rating'    => 5,
        ],
        [
            'name'      => 'Gafur',
            'game_name' => 'Call Of Duty',
            'rating'    => 5,
        ],
        [
            'name'      => 'Anhar',
            'game_name' => 'Among Us',
            'rating'    => 5,
        ],
        [
            'name'      => "Bagus kurniawan",
            'game_name' => 'Mobile Legends',
            'rating'    => 4,
        ],
        [
            'name'      => "Muhammad Fahrul",
            'game_name' => 'Call of Duty',
            'rating'    => 5,
        ],
        [
            'name'      => "Afzal mannaf",
            'game_name' => 'Among Us',
            'rating'    => 4,
        ],
        [
            'name'      => "Muhammad Azzam",
            'game_name' => 'League Of Legends',
            'rating'    => 4,
        ],
        [
            'name'      => "Hairul umam",
            'game_name' => 'Free Fire',
            'rating'    => 2,
        ],
        [
            'name'      => "Fajrul Imam",
            'game_name' => 'Among Us',
            'rating'    => 2,
        ],
        [
            'name'      => "Aris Munandar",
            'game_name' => 'Free Fire',
            'rating'    => 1,
        ],
        [
            'name'      => "Muhammad Ma'ruf",
            'game_name' => 'Call Of Duty',
            'rating'    => 4,
        ],
        [
            'name'      => "Ismu abil",
            'game_name' => 'Mobile Legends',
            'rating'    => 5,
        ],
        [
            'name'      => "Altab Rasyid",
            'game_name' => 'Among Us',
            'rating'    => 5,
        ],
        [
            'name'      => "Bayu Siswanto",
            'game_name' => 'Among Us',
            'rating'    => 4,
        ],
        [
            'name'      => "Adim juliadin",
            'game_name' => 'Mobile Legends',
            'rating'    => 2,
        ],
        [
            'name'      => "Dimas Anggara",
            'game_name' => 'Lords Mobile',
            'rating'    => 2,
        ],
        [
            'name'      => "Siswandi",
            'game_name' => 'Among Us',
            'rating'    => 1,
        ],
        [
            'name'      => "Abdul Halik",
            'game_name' => 'League Of Legends',
            'rating'    => 2,
        ],
        [
            'name'      => "Danang wahyudi",
            'game_name' => 'Among Us',
            'rating'    => 5,
        ],
        [
            'name'      => "Talisa",
            'game_name' => 'Call Of Duty',
            'rating'    => 2,
        ],
        [
            'name'      => "Selfi sulastri",
            'game_name' => 'Among Us',
            'rating'    => 3,
        ],
        [
            'name'      => "Muhammad Firdas",
            'game_name' => 'PUBG Mobile',
            'rating'    => 1,
        ],
        [
            'name'      => "Sunardin",
            'game_name' => 'Free Fire',
            'rating'    => 1,
        ],
        [
            'name'      => "Reza",
            'game_name' => 'Call Of Duty',
            'rating'    => 3,
        ],
        [
            'name'      => "Muamar setiawan",
            'game_name' => 'Among Us',
            'rating'    => 3,
        ],
        [
            'name'      => "Fery mansur",
            'game_name' => 'Free Fire',
            'rating'    => 5,
        ],

    ];

    public function index()
    {
        $userName = $this->getUsernameFromDataTesting();
        $userIds  = User::query()->whereIn('name', $userName)->get()->pluck('id')->toArray();
        // dd($userIds);

        $recommender = new CollaborativeFiltering();
        $recommender->makeRecommendation();
        $recommender->makePrediction($userIds);


        $gameNames = $this->getGamenameFromDataTesting();

        $gameIds = Game::query()
            ->whereIn('name', $gameNames)
            ->get()
            ->pluck('id')
            ->toArray();

        $predictions = Prediction::query()
            ->with('user')
            ->whereIn('game_id', $gameIds)
            ->whereIn('user_id', $userIds)
            ->get();


        $userPredicted = $predictions->map(function ($prediction) {
            return $prediction->user->name;
        })->toArray();

        $actualPrediction = collect($this->dataTesting)->filter(function ($userTesting)
        use ($userPredicted) {
            return in_array($userTesting['name'], $userPredicted);
        })->map(function ($userTesting) {
            return $userTesting['rating'];
        })->toArray();

        // dd($predictions, $actualPrediction);
        $mae = $recommender->MAEResult($predictions->toArray(), array_values($actualPrediction));

        return view('admin.testing.result', compact('mae'));
    }

    private function deleteRating()
    {
        $gameid = Game::query()->where('name', 'PUBG Mobile')->first()->id;

        foreach ($this->dataTesting as $key => $data) {
            $user = User::query()->where('name', $data['name'])->first();
            $user->ratings()->where('game_id', $gameid)->delete();
        }
    }

    private function getPredictingGameId()
    {
        $unratedGameIds = [];
        // list()
        $gameid = Game::query()->where('name', 'PUBG Mobile')->first()->id;

        foreach ($this->dataTesting as $key => $data) {
            $user = User::query()->where('name', $data['name'])->first();
            $ratedGames = $user->ratings->pluck('game_id')->toArray();
            $userUnratedGameIds = Game::query()->whereNotIn('id', $ratedGames)->get()->pluck('id')->toArray();
            $unratedGameIds[] = $userUnratedGameIds;
        }

        return array_intersect(...$unratedGameIds);
    }

    private function determineHasNeighbor($gameId)
    {
        $previousGame = Game::query()->where('id', '<', $gameId)->first();
        $nextGame     = Game::query()->where('id', '>', $gameId)->first();

        if ($nextGame) return [$previousGame->id, $nextGame->id];

        return false;
    }

    public function clearRating($user)
    {
        $user->ratings()->delete();
    }

    private function getRandomGame($dataTesting)
    {
        $this->gameNames = array_keys($dataTesting);
        $this->gameNames = array_slice($this->gameNames, 1);

        return Game::whereNotIn('name', $this->gameNames)->first()->id;
    }

    private function getUsernameFromDataTesting()
    {
        return array_map(function ($user) {
            return $user['name'];
        }, $this->dataTesting);
    }

    private function getGamenameFromDataTesting()
    {
        return array_map(function ($user) {
            return $user['game_name'];
        }, $this->dataTesting);
    }
    public function getRatingPrediction()
    {
    }
}
