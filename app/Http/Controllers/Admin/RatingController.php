<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Rating;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\ImportRequest;
use App\Imports\RatingImport;
use Maatwebsite\Excel\Facades\Excel;

class RatingController extends Controller
{
    public function index()
    {
        $ratings = Rating::with(['user', 'game'])->get();

        return view('admin.rating.index', compact('ratings'));
    }

    public function showFormImport()
    {
        return view('admin.rating.import');
    }

    public function import(ImportRequest $request)
    {
        Excel::import(new RatingImport, $request->file('excel'));

        return redirect()->route('admin.rating.index');
    }
}
