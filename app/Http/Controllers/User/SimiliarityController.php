<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Game;
use App\Models\Rating;
use App\Models\Similiarity;
use App\Recommender\CollaborativeFiltering;
use Illuminate\Http\Request;

class SimiliarityController extends Controller
{
    public function index()
    {
        $recommender = new CollaborativeFiltering();
        $recommender->makeRecommendation();
    }
}
