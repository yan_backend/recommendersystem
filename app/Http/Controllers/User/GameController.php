<?php

namespace App\Http\Controllers\User;

use App\Events\Rated;
use App\Http\Controllers\Controller;
use App\Models\Game;
use App\Models\Prediction;
use App\Models\Rating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GameController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $ratedGame = Rating::where('user_id', Auth::id())
            ->get()
            ->pluck('game_id')
            ->toArray();


        $predictionGames = Auth::user()->predictions;

        $games = Game::whereNotIn('id', $ratedGame)->get();

        return view('user.index', compact('games', 'predictionGames'));
    }

    public function rate(Request $request)
    {
        $this->validate($request, [
            'rating' => 'required|min:1',
            'id'     => 'required|exists:games'
        ]);

        $ratingExists = Rating::where('user_id', Auth::id())
            ->where('game_id', $request->id)
            ->exists();

        if ($ratingExists) {
            return response()->json([
                'message' => 'rating for this game already exists'
            ], 400);
        }

        Rating::create([
            'rating'  => $request->rating,
            'game_id' => $request->id,
            'user_id' => Auth::id()
        ]);

        event(new Rated($request->id));

        return response()->json([
            'success' => 'rate success'
        ], 200);
    }
}
