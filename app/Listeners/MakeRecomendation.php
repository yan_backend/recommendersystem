<?php

namespace App\Listeners;

use App\Models\Prediction;
use App\Recommender\CollaborativeFiltering;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

class MakeRecomendation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $this->deletePrediction($event->gameId);

        $recomendation = new CollaborativeFiltering();
        $recomendation->makeRecommendation();
        $recomendation->makePrediction(Auth::id());
    }

    private function deletePrediction($gameId)
    {
        Prediction::query()
            ->where('game_id', $gameId)
            ->where('user_id', Auth::id())
            ->delete();
    }
}
