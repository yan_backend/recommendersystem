<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'User\GameController@index')->name('index');
Route::post('/rate', 'User\GameController@rate');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// for admin
Route::namespace('Admin')->name('admin.')->middleware(['auth', 'admin'])->prefix('admin')->group(function () {
    Route::get('/', 'HomeController@index')->name('index');
    Route::get('ratings', 'RatingController@index')->name('rating.index');
    Route::get('ratings/import', 'RatingController@showFormImport')->name('rating.show.import');
    Route::post('ratings/import', 'RatingController@import')->name('rating.import');
    Route::get('testing', 'TestingController@index')->name('testing.index');
    Route::get('predictions', 'PredictionController@index')->name('prediction.index');

    Route::resource('games', GameController::class);
});

Route::get('similiarity', 'User\SimiliarityController@index');
