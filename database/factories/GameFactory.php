<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Game;
use Faker\Generator as Faker;

$factory->define(Game::class, function (Faker $faker) {
    $imageName = $faker->image('public/images');

    return [
        'name'        => $faker->title,
        'genre'       => $faker->text(10),
        'description' => $faker->text(30),
        'image'       => str_replace('public/images\\', '', $imageName)
    ];
});
