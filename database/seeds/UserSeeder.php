<?php

use App\Models\Game;
use App\Models\Rating;
use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ratingScores = [
            [
                'Mobile Legends Bang Bang' => 5,
                'PUBG Mobile' => 5,
                'Free Fire' => 3,
                'Rise Of Kingdoms' => 2,
                'Among Us' => 3,
                'League Of Legends' => 1,
                'Call Of Duty' => 4,
                'Lords Mobile' => 2
            ],
            [
                'Mobile Legends Bang Bang' => 4,
                'PUBG Mobile' => 5,
                'Free Fire' => 4,
                'Genshin Impact' => 5,
                'Rise Of Kingdoms' => 4,
                'Among Us' => 4,
                'Lineage 2 Revolution' => 4,
                'League Of Legends' => 4,
                'Call Of Duty' => 4,
                'Lords Mobile' => 4,
            ],
            [
                'Mobile Legends Bang Bang' => 5,
                'PUBG Mobile' => 2,
                'Genshin Impact' => 1,
                'Rise Of Kingdoms' => 1,
                'Among Us' => 1,
                'Lineage 2 Revolution' => 1,
                'League Of Legends' => 4,
                'Call Of Duty' => 4,
                'Lords Mobile' => 2,
            ],
            [
                'Mobile Legends Bang Bang' => 4,
                'PUBG Mobile' => 3,
                'Free Fire' => 3,
                'Genshin Impact' => 4,
                'Rise Of Kingdoms' => 3,
                'Among Us' => 4,
                'Lineage 2 Revolution' => 3,
                'League Of Legends' => 4,
                'Call Of Duty' => 4,
                'Lords Mobile' => 3,
            ],
            [
                'Mobile Legends Bang Bang'  => 3,
                'PUBG Mobile' => 4,
                'Free Fire' => 3,
                'Genshin Impact' => 2,
                'Rise Of Kingdoms' => 1,
                'Among Us' => 3,
                'Lineage 2 Revolution' => 2,
                'League Of Legends' => 3,
                'Call Of Duty' => 2,
                'Lords Mobile' => 5,
            ],
        ];

        // 'Mobile Legends Bang Bang'
        // 'PUBG Mobile'
        // 'Free Fire'
        // 'Genshin Impact'
        // 'Rise Of Kingdoms'
        // 'Among Us'
        // 'Lineage 2 Revolution'
        // 'League Of Legends'
        // 'Call Of Duty'
        // 'Lords Mobile'
        // factory(User::class, 5)->create()->each(function ($user, $index)
        // use ($ratingScores) {
        //     foreach ($ratingScores[$index] as $gameName => $rating) {
        //         $gameId = Game::whereName($gameName)->first()->id;
        //         $user->ratings()->create([
        //             'game_id' => $gameId,
        //             'rating'  => $rating
        //         ]);
        //     }
        // });
        // $user = factory(User::class)->make();
        // $user->save();
        // $user->ratings()->create([
        //     'game_id' => rand(1, 5),
        //     'rating'  => 4
        // ]);
    }
}
