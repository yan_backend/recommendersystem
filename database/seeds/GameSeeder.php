<?php

use App\Models\Game;
use Illuminate\Database\Seeder;

class GameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gameNames = [
            'Mobile Legends Bang Bang',
            'PUBG Mobile',
            'Free Fire',
            'Genshin Impact',
            'Rise Of Kingdoms',
            'Among Us',
            'Lineage 2 Revolution',
            'League Of Legends',
            'Call Of Duty',
            'Lords Mobile'
        ];

        foreach ($gameNames as $key => $gameName) {
            factory(Game::class)->create([
                'name' => $gameName
            ]);
        }
    }
}
