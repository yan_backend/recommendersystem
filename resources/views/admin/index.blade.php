@extends('admin.shared.base')

@section('content')
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="card card-statistic-2">
                <div class="card-icon shadow-primary bg-primary">
                    <i class="fas fa-user"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Respondens</h4>
                    </div>
                    <div class="card-body">
                        {{ $userCount }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="card card-statistic-2">

                <div class="card-icon shadow-primary bg-primary">
                    <i class="fas fa-gamepad"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Games</h4>
                    </div>
                    <div class="card-body">
                        {{ $gameCount }}
                    </div>
                </div>
            </div>

        </div>
        <div class="col-lg-4 col-md-4 col-sm-12">

        </div>
    </div>

@endsection
