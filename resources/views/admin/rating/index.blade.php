@extends('admin.shared.base')

@section('content')
    <table class="table">
        <thead>
            <tr>
                <th>No</th>
                <th>User</th>
                <th>Game</th>
                <th>Rating</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($ratings as $rating)
                <tr>
                    <td>
                        {{ $loop->iteration }}
                    </td>
                    <td>
                        {{ $rating->user->name }}
                    </td>
                    <td>
                        {{ $rating->game->name }}
                    </td>
                    <td>
                        {{ $rating->rating }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <!-- Modal -->

@endsection
