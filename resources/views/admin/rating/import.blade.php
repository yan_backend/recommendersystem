@extends('admin.shared.base')

@section('content')
    <form action="{{ route('admin.rating.import') }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('POST')

        <div class="form-group">
            <input type="file" name="excel" id="excel" class="form-control">
        </div>
        <button type="submit" class="btn btn-sm btn-success">import</button>
    </form>

@endsection
