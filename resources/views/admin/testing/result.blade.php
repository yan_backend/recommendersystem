@extends('admin.shared.base')

@section('content')
    <!-- Button trigger modal -->
    <div class="row d-flex justify-content-center">
        <div class="col-md-6">
            <div class="card p-10">
                <div class="card-body">
                    <div class="card-title">
                        <h5>Hasil Testing</h5>
                    </div>
                    {{ $mae }}
                </div>
            </div>
        </div>
    </div>
@endsection
