@extends('admin.shared.base')

@section('content')
    <!-- Button trigger modal -->
    <a class="btn btn-primary float-right" href="{{route('admin.games.create')}}">
      Add New Game
    </a>

    <table class="table">
        <thead>
            <tr>
                <th>
                    No
                </th>
                <th>
                    Name
                </th>
                <th>
                    Image
                </th>
                <th>
                    Genre
                </th>
                <th>
                    Description
                </th>
                <th>
                    Action
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($games as $game)
                <tr>
                    <td>
                        {{$loop->iteration}}
                    </td>
                    <td>
                        {{$game->name}}
                    </td>
                    <td>
                        <img style="width: 100px" src="{{asset('images/'.$game->image)}}" alt="" srcset="">
                    </td>
                    <td>
                        {{$game->genre}}
                    </td>
                    <td>
                        {{$game->description}}
                    </td>
                    <td>
                        <a class="btn tbn-sm btn-warning" href="{{route('admin.games.edit', ['game'=>$game->id])}}">
                            Edit
                        </a>
                        <form action="{{route('admin.games.destroy', ['game'=>$game->id])}}" method="POST" class="form-inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn tbn-sm btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <!-- Modal -->
  
@endsection
 