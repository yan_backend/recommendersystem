@extends('admin.shared.base')

@section('content')
     <div class="card">
       <div class="card-body">
             <form action="{{route('admin.games.update', ['game'=>$game->id])}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="name">
                            Name
                        </label>
                        <input type="text" class="form-control" name="name" id="name" value="{{old('name')?:$game->name?:''}}">
                        @error('name')
                            <span class="small alert alert-danger">
                                {{$message}}
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="genre">
                            Genre
                        </label>
                        <input type="text" class="form-control" name="genre" id="genre" value="{{old('genre')?:$game->genre?:''}}">
                        @error('genre')
                            <span class="small alert alert-danger">
                                {{$message}}
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="description">
                            Description
                        </label>
                        <textarea class="form-control" name="description" id="description" cols="30" rows="10">{{old('description')?:$game->description?:''}}</textarea>
                        @error('description')
                            <span class="small alert alert-danger">
                                {{$message}}
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <img src="{{asset('images/'.$game->image)}}" alt="">
                        <label for="Image">
                            Image    
                        </label>
                        <input type="file" class="form-control" name="image" id="image">
                        @error('image')
                            <span class="small alert alert-danger">
                                {{$message}}
                            </span>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
       </div>
   </div>
@endsection