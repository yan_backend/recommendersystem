@extends('admin.shared.base')

@section('content')
    <!-- Button trigger modal -->
    <a class="btn btn-primary float-right" href="{{ route('admin.games.create') }}">
        Add New Game
    </a>

    <table class="table">
        <thead>
            <tr>
                <th>
                    No
                </th>
                <th>
                    User Name
                </th>
                <th>
                    Game
                </th>
                <th>
                    Score
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($predictions as $prediction)
                <tr>
                    <td>
                        {{ $loop->iteration }}
                    </td>
                    <td>
                        {{ $prediction->user->name }}
                    </td>
                    <td>
                        {{ $prediction->game->name }}
                    </td>
                    <td>
                        {{ $prediction->prediction_score }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <!-- Modal -->

@endsection
