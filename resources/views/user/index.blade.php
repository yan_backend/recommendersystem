@extends('layouts.app')

@section('content')

    @if (count($predictionGames) > 0)
        <h2>Game Yang Mungkin Anda Suka</h2>
        <div class="row p-10">
            @foreach ($predictionGames as $prediction)
                <div class="col-md-3 mb-10">
                    <img src="{{ asset('images/' . $prediction->game->image) }}" alt="" class="card-img-top">
                    <div class="card-body">
                        <h4 class="card-title">
                            {{ $prediction->game->name }}
                        </h4>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
    <h2>Game Lainnya</h2>
    <div class="row p-10">
        @foreach ($games as $game)
            <div class="col-md-3 mb-10">
                <img src="{{ asset('images/' . $game->image) }}" alt="" class="card-img-top">
                <div class="card-body">
                    <h4 class="card-title">
                        {{ $game->name }}
                    </h4>
                    <div class="d-flex justify-content-between">
                        <span><i class="fas fa-star" data-rating="1" data-id="{{ $game->id }}"></i></span>
                        <span><i class="fas fa-star" data-rating="2" data-id="{{ $game->id }}"></i></span>
                        <span><i class="fas fa-star" data-rating="3" data-id="{{ $game->id }}"></i></span>
                        <span><i class="fas fa-star" data-rating="4" data-id="{{ $game->id }}"></i></span>
                        <span><i class="fas fa-star" data-rating="5" data-id="{{ $game->id }}"></i></span>
                    </div>
                    <form action="#" method="POST">
                        @csrf
                        @method('POST')
                        <input type="hidden" id="score_{{ $game->id }}" name="score_{{ $game->id }}">
                    </form>
                </div>
            </div>
        @endforeach
    </div>
@endsection

@section('js')
    <script defer>
        var oneScore = false;
        $('.fa-star').click(function(event) {
            let score = event.target.dataset.rating;
            let id = event.target.dataset.id;
            oneScore = score != 1 ? false : true;

            if (oneScore && (event.target.style.color == 'yellow')) {
                clean_rating(id)
                // set_score()
                return false;
            }

            clean_rating(id)
            set_rating(score, id)
            set_score(score, id)
            send_rating(score, id)
        })

        function set_rating(amount, id) {
            let $ratingsElems = $('[data-id=' + id + ']');

            $ratingsElems.each(function(index, elem) {
                if ((index) == amount) {
                    return false;
                }

                $(elem).css('color', 'yellow')
            })
        }

        function clean_rating(id) {
            let $ratingsElems = $('[data-id=' + id + ']');
            $ratingsElems.each(function(index, elem) {
                $(elem).css('color', 'black')
            })
        }

        function set_score(score = 0, id) {
            $('#score_' + id).val(score)

            console.log($('#score_' + id).val());
        }

        function send_rating(rating, id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                data: {
                    rating,
                    id
                },
                type: "POST",
                url: "rate",
                dataType: "json",
                success: function(success) {
                    alert(success.success);
                },
                error: function(error) {
                    console.error(error);
                    alert(error.responseJSON.message)
                }
            })
        }
    </script>
@endsection
